#ifndef __COMPONENT__
#define __COMPONENT__

#include <iostream>
#include <iomanip>
#include <vector>
#include <string>

class Component {
    public:
        virtual void show() = 0;
} 